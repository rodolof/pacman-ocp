# pacman-ocp

This is a simple pacman app that utilizes the default storage class in OCP for storing high score. 

```
git clone https://gitlab.com/rodolof/pacman-ocp
cd pacman-ocp
oc create -f .
oc get route -n pacman
```
Play pacman

Please ensure you don't have another one running, or you will have problems with the routes...

Forked from https://github.com/dav1x/pacman-ocp
